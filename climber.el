;;; climber.el --- Interact with CLI programs -*- lexical-binding: t -*-

;; Copyright (C) 2023 John Task

;; Author: John Task <q01@disroot.org>
;; Maintainer: John Task <q01@disroot.org>
;; Version: 0.2
;; Package-Requires: ((emacs "26.1") (transient "0.3.7") (compat "28.1.2.1"))
;; Keywords: extensions
;; URL: https://gitlab.com/q01_code/climber

;; This file is not part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; climber is an attempt to support a wide range of CLI programs in
;; Emacs using the excelent transient library.  Think about it as
;; Magit, but for all your regular, shell-based programs.

;;; Code:

(require 'compat)
(require 'transient)

(defun climber-read-argument (prompt _initial-input _history)
  "Read a string, quoting any special character."
  (shell-quote-argument (read-from-minibuffer prompt)))

(defun climber-read-file (prompt _initial-input _history)
  "Read a file, quoting any special character."
  (shell-quote-argument
   (file-local-name (expand-file-name (read-file-name prompt)))))

(defun climber-read-existing-file (prompt _initial-input _history)
  "Read an existing file, quoting any special character."
  (shell-quote-argument
   (file-local-name (expand-file-name (read-file-name prompt nil nil t)))))

(defun climber-read-directory (prompt _initial-input _history)
  "Read a directory, quoting any special character."
  (shell-quote-argument
   (file-local-name (expand-file-name (read-directory-name prompt)))))

(defun climber-read-url-multi (prompt _initial-input _history)
  "Read multiple URLs.  Return the URLs, separated by a whitespace."
  (string-join (completing-read-multiple prompt nil) " "))

;; Helpers

(defun climber-find-place (n key args)
  "Return the Nth element of the list whose car is KEY in ARGS."
  (let (result)
    (dolist (this-list args result)
      (when (equal (car this-list) key)
        (setq result (nth n this-list))))))

(defun climber-add-incompatible-section (args)
  "Return a list with a :incompatible section according to ARGS."
  (if-let ((incompatible (plist-get args :incomp))
           (args (plist-get args :args)))
      ;; There's an incompatible section; proceed
      (let (final-result temp-result)
        (dolist (this-list incompatible final-result)
          (when (stringp (car this-list))
            ;; This is a valid list
            (dolist (key this-list temp-result)
              (push (climber-find-place 2 key args) temp-result)))
          (push temp-result final-result))
        `(:incompatible ',final-result))
    ;; If there's no :incomp keyword, we return :incompatible nil.
    ;; It would be nice to return nothing, but that's not possible
    ;; as far as I know.
    (list :incompatible nil)))

;; Definition

;;;###autoload
(defmacro climber-def (name &rest args)
  "Define a new climber named NAME.
ARGS are used to configure climber."
  ;; Macros are _hard_.  I try to keep things
  ;; sane here, but I'm not that good.
  (declare (indent defun))
  `(progn
     ;; Our function will be named by prepending climber- to the symbol name
     (transient-define-prefix ,(intern (concat "climber-" (symbol-name name))) ()
       ;; Documentation string
       ,(format "Climber support for %s." (symbol-name name))
       ;; Incompatible switches
       ,@(climber-add-incompatible-section args)
       ;; Actual transient definition
       ,(vector
         ;; Title
         (concat (or (plist-get args :title) (symbol-name name)) "\n")
         ;; Arguments
         (let (arguments temp-list)
           (dolist (element (plist-get args :args))
             ;; Store the first three slots of this argument definition
             (push (take 3 element) temp-list)
             ;; Now add the rest of options, according to last slot
             (setq
              temp-list
              (pcase (car (last element))
                ('string
                 (append
                  temp-list
                  (list :prompt "String: "
                        :class 'transient-option
                        :reader 'climber-read-argument)))
                ('url
                 (append
                  temp-list
                  (list :prompt "URL: "
                        :class 'transient-option
                        :unsavable t
                        :always-read t)))
                ('url-multi
                 (append
                  temp-list
                  (list :prompt "URL: "
                        :class 'transient-option
                        :unsavable t
                        :always-read t
                        :reader 'climber-read-url-multi)))
                ('toggle
                 (append
                  temp-list
                  (list :class 'transient-switch)))
                ('toggle-default
                 (append
                  temp-list
                  (list :class 'transient-switch)))
                ('file
                 (append
                  temp-list
                  (list :prompt "File: "
                        :reader 'climber-read-file
                        :class  'transient-option)))
                ('existing-file
                 (append
                  temp-list
                  (list :prompt "File: "
                        :reader 'climber-read-existing-file
                        :class  'transient-option)))
                ('dir
                 (append
                  temp-list
                  (list :prompt "Directory: "
                        :reader 'climber-read-directory
                        :class  'transient-option)))
                ('natnum
                 (append
                  temp-list
                  (list :prompt "Number: "
                        :reader 'transient-read-number-N+
                        :class  'transient-option)))
                ('num
                 (append
                  temp-list
                  (list :prompt "Number: "
                        :reader 'transient--read-number-N
                        :class  'transient-option)))
                (_
                 (cond ((equal (substring (nth 2 element) -1) "=")
                        ;; The last character is '=', so we expect a string
                        (append
                         temp-list
                         (list
                          :prompt "String: "
                          :class 'transient-option
                          :reader 'climber-read-argument)))
                       (t
                        ;; Otherwise, we expect a toggle
                        (append
                         temp-list
                         (list :class 'transient-switch)))))))
             ;; We "flatten" the list because it cannot be used as-is
             (setq temp-list (flatten-tree temp-list))
             ;; Add init values. We don't do this before because flatten-tree
             ;; doesn't like lambdas.
             (pcase (car (last element))
               ((or 'url 'url-multi)
                (setq temp-list
                      (append
                       temp-list
                       '(:init-value
                         (lambda (obj)
                           (oset
                            obj value
                            (when (string-match
                                   "^http[s]?"
                                   (substring-no-properties (current-kill 0)))
                              (substring-no-properties (current-kill 0)))))))))
               ('toggle-default
                (setq temp-list
                      (append
                       temp-list
                       '(:init-value
                         (lambda (obj)
                           (oset obj value (oref obj argument))))))))
             ;; Now we push our temporal list to the arguments
             (push temp-list arguments)
             ;; Reset temporal list
             (setq temp-list nil))
           ;; The arguments are there, but reversed because of push.
           ;; We change that here.
           (setq arguments (reverse arguments))
           ;; We create a vector as expected by transient
           (vconcat (push "Arguments" arguments))))
       ;; Run section
       ,(vector (vector
                 "End"
                 '("SPC" "Preview final command" climber-print-command)
                 '("RET" "Run" climber-run)))
       ;; Body
       (interactive)
       (transient-setup
        (intern ,(concat "climber-" (symbol-name name)))
        ;; Scope holds the command to run
        nil nil :scope ,(or (plist-get args :run) (symbol-name name))))
     ;; Our setup worked (hopefully), so bind the transient
     (when-let ((bind ,(plist-get args :bind)))
       (global-set-key
        (kbd bind) #',(intern (concat "climber-" (symbol-name name)))))))

(transient-define-suffix climber-print-command (&rest _)
  "Report the current state of command."
  :transient 'transient--do-call
  (interactive)
  (let ((args (transient-args (oref transient-current-prefix command)))
        (program (oref transient-current-prefix scope)))
    (message "%s %s" program (or (string-join args " ") ""))))

(transient-define-suffix climber-run (args)
  "Run a CLI program with ARGS."
  (interactive (list (transient-args transient-current-command)))
  (let ((program (oref transient-current-prefix scope)))
    (async-shell-command
     (if (not args)
         program
       (concat program " " (string-join args " ")))
     (concat "*" program "*"))))

(provide 'climber)

;;; climber.el ends here
